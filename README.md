# my-magic-prompt by David PINHO
___
## help
     Indicates the commands you can use
___
## ls
     List files and folders visible as hidden
___
## rm
     Delete a file
___
## rmd or rmdir
     Delete a folder
___
## about 
     A description of your programme
___
## version or --v or vers
     Displays the version of your prompt
___
## age
     Asks you your age and tells you whether you are an adult or a minor
___
## quit
     allows you to leave the prompt
___
## profile
     allows you to display all the information about yourself.
___
## passw
     permet de changer le password avec une demande de confirmation
___
## cd
     go to a folder you have just created or return to a previous folder previous
___
## pwd
     indicates the current directory
___
## hour
      allows you to give the current time
___
## *
     indicate an unknown command
___
## httpget 
     allows you to download the html source code of a web page and save it in a specific file. Your prompt should ask you what the file name will be
___
## smtp 
     allows you to send a mail with an address, a subject and the mail body
___
## open
     open a file directly in the VIM editor even if the file does not exist
___

