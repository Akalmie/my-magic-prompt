#!/usr/bin/bash
help_prog(){
    echo "-help : print the help
-ls : show the files
-rm : delete file
-rmd or rmdir : delete directory
-about : information about the prog
-version | vers | --v : prog's version
-age : ask your age and tells you if you're minor or minor
-quit : exit
-profile : print dev's profile
-passw : change password
-cd : move into directories
-pwd : print the current path
-hour : print hour and minutes
-* : unknow command
-httpget : download http code form url
-smtp : send a mail
-open : open a file with VIM even if it doesn't exist
"
}
about_prog(){
    echo"a shell program that allows the user to access certain basic functions through commands"
}
age_usr(){
    echo "How old are you ? :"
    read usrage
        re='^[0-9]+$'
        if ! [[ $usrage =~ $re ]] ; then
        echo "error no characters or negative numbers allowed" >&2; 
        else if [ "$usrage" -ge 18 ]; then
            echo "You are an adult"
        else 
            echo "You are minor"
    fi
    fi
}
profile_usr(){
    echo "First Name : David Last Name : PINHO Age : 25 Mail : sendmailtestubuntu@gmail.com"
}
hour_prog(){
    date +"%H:%M"
}
password_change(){
    echo "your current pass"
    read -s  passusr
    if [ "$passusr" != "$password" ]; then
        echo "incorrect password"
    else 
        echo "new password : "
        read -s confirmpassword1
        echo "confirm you password"
        read -s confirmpassword2
    if [ "$confirmpassword1" != "$confirmpassword2" ]; then
        echo "not same password"
    else 
        password=$confirmpassword2
        echo $password
    fi
    fi
}
httpget_prog(){
    echo "name file : "
    read filename
    touch $filename
    echo "URL : "
    read url
    wget $url -O $filename
}
postfix_mail(){
    echo "Mail to :"
    read mail
    echo "Subject :"
    read subject
    echo "Body :"
    read body
    echo "$body" | mail -s "$subject" $mail
}
prompt(){
    while (true)
    do
        echo "Enter your command :"
        read commande
        clear
        echo "command : " $commande    
        args=( $commande )    
        case "${args[0]}" in 
            help ) help_prog;;    
            ls  ) ls -a ;;
            rm  ) rm "${args[1]}" ;;
            rmd | rmdir ) rm -rf "${args[1]}" ;; 
            about ) about_prog ;;
            version | --v | vers ) echo "1.0";;
            age ) age_usr ;;
            quit ) exit 1 ;;
            profile ) profile_usr ;;
            passw ) password_change "${args[1]}";;
            pwd ) pwd ;;
            cd ) cd "${args[1]}" ;;
            hour ) hour_prog ;;
            httpget ) httpget_prog ;;
            smtp ) postfix_mail ;;
            open ) vim "${args[1]}" ;;
            * ) echo "Command not found" ;;
        esac
    done
}
main() {
    echo -n "Login : "
    read login
    echo -n "Password : "
    read -s password
    if [ "$login" != "david" ] || [ "$password" != "david" ]; then
        echo "Invalid login or password"
        exit
    fi    
    echo "Welcome" $login;
   prompt
    exit
}

main 
